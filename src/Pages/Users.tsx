import { NavLink } from "react-router-dom";
import { UserCard } from "../Components/UserCard";
import { Routes } from "../Constants";
import { useUsers } from "../Hooks";

export const Users = () => {
  const { users } = useUsers()!;
  if (users.length === 0) {
    return (
      <div className="h-screen w-full flex place-items-center justify-center">
        No users yet &nbsp;
        <NavLink className="text-blue-500" to={Routes.createUser}>
          Click here to create the first one!
        </NavLink>
      </div>
    );
  }
  return (
    <div>
      <div className="text-xl font-bold pb-4">Users</div>
      <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-4">
        {users.map((user) => (
          <UserCard user={user} key={`${user.name}-${user.email}`} />
        ))}
      </div>
    </div>
  );
};
