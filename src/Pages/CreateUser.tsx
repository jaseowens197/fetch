import { FormEvent } from "react";
import { FormInput, Loading } from "../Components";
import { useApi } from "../Hooks";
import { User } from "../Types";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { Routes } from "../Constants";

export const CreateUser = () => {
  const navigate = useNavigate();
  const { useGetFormData, useCreateUser } = useApi();
  const { data, isLoading, isError } = useGetFormData();

  const handleCreateUser = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const formData = new FormData(event.target as HTMLFormElement);
    const data: User = Object.fromEntries(formData.entries());

    useCreateUser.mutate(data, {
      onSuccess: (_, vars) => {
        toast.success(`${vars.name} was succesfully created!`);
        navigate(Routes.users);
      },
      onError: () => {
        toast.error("Uh-oh something went wrong creating your user!");
      },
    });
  };

  if (isLoading) {
    return (
      <div className="flex justify-center w-full h-screen place-items-center">
        <Loading />
      </div>
    );
  }
  if (isError) {
    return (
      <div className="flex justify-center w-full h-screen place-items-center">
        Uh oh! Looks like something has gone wrong, please try again later.
      </div>
    );
  }
  return (
    <div className="w-full flex justify-center">
      <div className="w-full max-w-5xl bg-white p-6 divide-y-2 rounded-lg shadow">
        <div className="text-lg pb-4">Create User</div>
        <form onSubmit={handleCreateUser}>
          <div className="grid gap-6 grid-cols-1 md:grid-cols-2 mt-4">
            <FormInput label="Name">
              <input
                required
                type="text"
                name="name"
                className="form-input mt-1 block w-full"
                placeholder="Jase Owens"
              />
            </FormInput>

            <FormInput label="Email">
              <input
                required
                type="email"
                name="email"
                className="form-input mt-1 block w-full"
                placeholder="jaseowens@test.com"
              />
            </FormInput>

            <FormInput label="Password">
              <input
                required
                type="password"
                name="password"
                className="form-input mt-1 block w-full"
                placeholder="*********"
              />
            </FormInput>

            <FormInput label="Occupation">
              <select
                name="occupation"
                className="form-input mt-1 block w-full"
              >
                {data.occupations.map((occupation) => (
                  <option key={occupation} value={occupation}>
                    {occupation}
                  </option>
                ))}
              </select>
            </FormInput>

            <FormInput label="State">
              <select name="state" className="form-input mt-1 block w-full">
                {data.states.map((state) => (
                  <option key={state.name} value={state.name}>
                    {state.abbreviation} - {state.name}
                  </option>
                ))}
              </select>
            </FormInput>

            <div className="col-span-1 md:col-span-2 flex justify-end">
              <input
                type="submit"
                value="Create!"
                className="bg-blue-500 text-white cursor-pointer border-2 rounded-lg px-4 py-2"
              />
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};
