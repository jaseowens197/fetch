import { createContext, ReactNode, useState } from "react";
import { User } from "../Types";

type Props = {
  children: ReactNode;
};

type UsersContextType = {
  users: User[];
  setUsers: React.Dispatch<React.SetStateAction<User[]>>;
};

export const UsersContext = createContext<UsersContextType | undefined>(
  undefined
);

export const UsersProvider = ({ children }: Props) => {
  const [users, setUsers] = useState<User[]>([]);

  return (
    <UsersContext.Provider value={{ users, setUsers }}>
      {children}
    </UsersContext.Provider>
  );
};
