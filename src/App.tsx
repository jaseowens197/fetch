import { Routes, Route } from "react-router-dom";
import { Layout } from "./Components";
import { CreateUser, NotFound, Users } from "./Pages";
import { Routes as AppRoutes } from "./Constants";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { UsersProvider } from "./Contexts";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";

const queryClient = new QueryClient();

export const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <UsersProvider>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Users />} />
            <Route path={AppRoutes.createUser} element={<CreateUser />} />
            <Route path="*" element={<NotFound />} />
          </Route>
        </Routes>
        <ToastContainer />
      </UsersProvider>
    </QueryClientProvider>
  );
};
