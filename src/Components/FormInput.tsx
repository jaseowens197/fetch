import { FC, ReactNode } from "react";

interface Props {
  label: string;
  children?: ReactNode;
}

export const FormInput: FC<Props> = ({ label, children }) => {
  return (
    <label className="block">
      <span className="text-gray-700">{label}</span>
      {children}
    </label>
  );
};
