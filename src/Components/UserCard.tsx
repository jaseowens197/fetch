import { FC } from "react";
import { User } from "../Types";

interface Props {
  user: User;
}

export const UserCard: FC<Props> = ({ user }) => {
  return (
    <div className="bg-white rounded-md shadow p-6">
      <div>
        <small>Name:</small> <strong>{user.name}</strong>
      </div>

      <div>
        <small>Email:</small> <strong>{user.email}</strong>
      </div>

      <div>
        <small>Occupation:</small> <strong>{user.occupation}</strong>
      </div>
      <div>
        <small>State:</small> <strong>{user.state}</strong>
      </div>
    </div>
  );
};
