import { Link, Outlet } from "react-router-dom";
import { Routes } from "../Constants";

export const Layout = () => {
  return (
    <div>
      <nav className="flex flex-col place-items-center text-center sm:flex-row sm:text-left sm:justify-between py-4 px-6 bg-white shadow w-full">
        <div className="mb-2 sm:mb-0">
          <Link
            className="font-bold text-gray-800 text-2xl hover:text-gray-500"
            to={Routes.users}
          >
            Fetch Users
          </Link>
        </div>
        <div>
          <Link
            to={Routes.createUser}
            className="text-lg text-gray-400 hover:text-gray-500"
          >
            Create
          </Link>
        </div>
      </nav>
      <hr />
      <div className="bg-gray-100 min-h-screen p-6">
        <Outlet />
      </div>
    </div>
  );
};
