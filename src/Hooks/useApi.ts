import { useUsers } from "./useUsers";
import { useMutation, useQuery } from "@tanstack/react-query";
import { BASE_ROUTE } from "../Constants";
import { User } from "../Types";

interface State {
  abbreviation: string;
  name: string;
}

export interface FetchFormData {
  occupations: string[];
  states: State[];
}

const FORM_QUERY_KEY = "formData";

export const useApi = () => {
  const { setUsers } = useUsers()!;

  const getFormData = () =>
    fetch(BASE_ROUTE + "/form").then(async (res) => await res.json());

  const useGetFormData = () =>
    useQuery<FetchFormData>([FORM_QUERY_KEY], getFormData);

  const createUser = (data: User) =>
    fetch(BASE_ROUTE + "/form", {
      method: "POST",
      mode: "cors",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });

  const useCreateUser = useMutation(createUser, {
    onSuccess: (_, vars) => {
      setUsers((prev) => [...prev, vars]);
    },
  });

  return {
    useGetFormData,
    useCreateUser,
  };
};
