import { useContext } from "react";
import { UsersContext } from "../Contexts";

export const useUsers = () => useContext(UsersContext);
